# main.py
# coding:utf-8
import os
import sys
import os.path
import shutil
import time
import datetime
import subprocess
import multiprocessing
import logging
from multiprocessing import Process, Queue, Manager

from werkzeug.utils import secure_filename
from werkzeug.datastructures import FileStorage
from flask import Flask, render_template, request, redirect, url_for, abort
from flask_uploads import UploadSet, configure_uploads, ALL
from flask_sqlalchemy import SQLAlchemy
from flask_security import Security, SQLAlchemyUserDatastore, \
    UserMixin, RoleMixin, login_required, current_user
from flask_security.utils import encrypt_password
import flask_admin
from flask_admin.contrib import sqla
from flask_admin import helpers as admin_helpers
from flask_admin import BaseView, expose

app = Flask(__name__)
app.config.from_pyfile('config.py')
db = SQLAlchemy(app)

logging.basicConfig(filename=app.config['OEM_LOGGING_FILE'], format=app.config['OEM_LOGGING_FORMAT'],
                    datefmt=app.config['OEM_LOGGING_DATEFMT'], level=app.config['OEM_LOGGING_LEVEL'])

oem_maker_task = multiprocessing.Manager().list()

lock = multiprocessing.Lock()


class Runner():
    @staticmethod
    def Runpipe(cmd):
        # print(cmd)
        output = subprocess.run(cmd, shell=True,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.STDOUT,
                                universal_newlines=True)
        if output.returncode != 0:
            return (False, output.stdout)
        else:
            return (True, output.stdout)


# Define models
roles_users = db.Table(
    'roles_users',
    db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
    db.Column('role_id', db.Integer(), db.ForeignKey('role.id'))
)


class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))

    def __str__(self):
        return self.name


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(255))
    last_name = db.Column(db.String(255))
    email = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean())
    confirmed_at = db.Column(db.DateTime())
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))

    def __str__(self):
        return self.email


class Taskinfo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), unique=True)
    type = db.Column(db.String(8))
    status = db.Column(db.String(8), default="init")

    active = db.Column(db.Boolean(), default=True)
    create_at = db.Column(db.DateTime(), default=datetime.datetime.utcnow)
    update_at = db.Column(db.DateTime(), default=datetime.datetime.utcnow)

    owner_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    owner = db.relationship(
        "User", backref=db.backref("user", lazy='dynamic'))


def __str__(self):
    return "{}:{}{}{}".format(self.id, self.name, self.type, self.status)


# Setup Flask-Security
user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)


class MyModelView(sqla.ModelView):

    def is_accessible(self):
        if not current_user.is_active or not current_user.is_authenticated:
            return False

        if current_user.has_role('superuser'):
            return True

        return False

    def _handle_view(self, name, **kwargs):
        """
        Override builtin _handle_view in order to redirect users when a view is not accessible.
        """
        if not self.is_accessible():
            if current_user.is_authenticated:
                # permission denied
                abort(403)
            else:
                # login
                return redirect(url_for('security.login', next=request.url))

    # can_edit = True
    edit_modal = True
    create_modal = True
    can_export = True
    can_view_details = True
    details_modal = True


class UserView(MyModelView):
    column_editable_list = ['email', 'first_name', 'last_name']
    column_searchable_list = column_editable_list
    column_exclude_list = ['password']
    # form_excluded_columns = column_exclude_list
    column_details_exclude_list = column_exclude_list
    column_filters = column_editable_list


# Create admin
admin = flask_admin.Admin(
    app,
    'My Dashboard',
    base_template='my_master.html',
    template_mode='bootstrap3',
)

# Add model views
admin.add_view(MyModelView(Role, db.session, menu_icon_type='fa',
                           menu_icon_value='fa-server', name="Roles"))
admin.add_view(UserView(User, db.session, menu_icon_type='fa',
                        menu_icon_value='fa-users', name="Users"))

# Create uploads
files = UploadSet('files', ALL)
configure_uploads(app, files)


@security.context_processor
def security_context_processor():
    return dict(
        admin_base_template=admin.base_template,
        admin_view=admin.index_view,
        h=admin_helpers,
        get_url=url_for
    )


def build_sample_db():
    """
    create admin && drop all data
    """

    import string
    import random

    db.drop_all()
    db.create_all()

    with app.app_context():
        user_role = Role(name='user')
        super_user_role = Role(name='superuser')
        db.session.add(user_role)
        db.session.add(super_user_role)
        db.session.commit()

        test_user = user_datastore.create_user(
            first_name='Admin',
            email='admin',
            password=encrypt_password('admin'),
            roles=[user_role, super_user_role]
        )

        test_task = Taskinfo(name="abc", type="oem", owner_id=test_user.id)
        test_task.owner = test_user
        db.session.add(test_task)
        db.session.commit()

    db.session.commit()
    return


@ app.route('/upload/<taskname>', methods=['GET', 'POST'])
@login_required
def upload(taskname):
    logging.debug("upload file to {} by {}".format(
        taskname, current_user))
    task_base_dir = "{}/{}/files".format(
        app.config['OEM_MM_TASK_WORKBASE'], taskname)
    if app.config['OEM_MM_TASK_WORKBASE'] and os.path.exists(app.config['OEM_MM_TASK_WORKBASE']):
        if not os.path.exists(task_base_dir):
            os.makedirs(
                task_base_dir, exist_ok=True)
    else:
        os.mkdir(app.config['OEM_MM_TASK_WORKBASE'])

    if request.method == 'POST' and 'media' in request.files:
        logging.debug("upload file to {} by {}".format(
            taskname, current_user))
        files.config.destination = task_base_dir
        files.save(request.files['media'])
        # filename = files.save(request.files['media'])
        # url = files.url(filename)

    idx_show_file_list = []
    for flist in os.listdir(task_base_dir):
        (_, _, _, _, _, _, size,
         _, _, ctime) = os.stat("{}/{}".format(task_base_dir, flist))
        idx_show_file_list.append(
            (flist, time.ctime(ctime), size / 1024 / 1024))
    return render_template('nupload.html', taskname=taskname, idx_file_list=idx_show_file_list)


@ app.route('/newtask/<isoname>', methods=['GET', 'POST'])
@login_required
def nettask(isoname):
    logging.debug("create newtask {} {}".format(isoname, request.method))
    if request.method == 'POST' or request.method == "GET":
        task_can_updated = False
        try:
            task_info = db.session.query(Taskinfo).filter(
                Taskinfo.name == isoname).one()

            if task_info.active:
                return redirect(url_for('submit', taskname=isoname))
        except:
            task_info = Taskinfo(name=isoname, type="oem",
                                 owner_id=current_user.id)
            task_info.owner = current_user
            db.session.add(task_info)
            db.session.commit()
        if not task_info:
            logging.debug("new taskinfo {} by {}".format(
                isoname, current_user.id))
            task_info = Taskinfo(name=isoname, type="oem",
                                 owner_id=current_user.id)
            task_info.owner = current_user
            db.session.add(task_info)
            db.session.commit()

    if task_info.status == "finish":
        task_can_updated = True

    return render_template('oem-type.html', taskinfo=task_info, canupdated=task_can_updated)


@ app.route('/', methods=['GET'])
@ app.route('/index', methods=['GET'])
def index():
    if request.method == 'GET':
        if current_user:
            logined = True
        else:
            logined = False
        return render_template('index.html', islogined=logined)


def iso_oem_maker(oem_maker_task, taskname):
    def _run(cmd, ignore_err=True):
        logging.info("Worker-{}#run {}".format(os.getpid(), cmd))
        ret, _ = Runner.Runpipe(cmd)
        if (not ret) and (not ignore_err):
            _failed()

    def _clear():
        if os.path.isfile(lock_file):
            os.unlink(lock_file)

    def _failed():
        logging.info("Worker-{}#failed {}".format(os.getpid(), taskname))
        task_info.status = "failed"
        task_info.update_at = datetime.datetime.utcnow()
        task_info.active = False
        db.session.add(task_info)
        db.session.commit()
        _clear()
        return

    def remove(t=oem_maker_task, n=taskname):
        if n in t:
            # i = t.index(n)
            lock.acquire()
            t.remove(n)
            lock.release()

    logging.info("Worker-{}#start {}{}".format(os.getpid(),
                                               taskname, oem_maker_task))
    maker_cancel = remove
    task_info = db.session.query(Taskinfo).filter(
        Taskinfo.name == taskname).one()
    lock_file = "{}/{}/.lockfile".format(
        app.config['OEM_MM_TASK_WORKBASE'], taskname)
    if os.path.isfile(lock_file):
        _failed()
    else:
        with open(lock_file, "w+") as f:
            f.write("\n")
            f.close()

    worker_base = "{}/{}/iosdir".format(
        app.config['OEM_MM_TASK_WORKBASE'], taskname)

    logging.info("Worker-{}#upack {}{}".format(os.getpid(),
                                               taskname, oem_maker_task))
    task_info.status = "upack"
    task_info.update_at = datetime.datetime.utcnow()
    db.session.add(task_info)
    db.session.commit()

    if os.path.exists("{}/{}.iso".format(app.config['OEM_MM_ISO_DIR'], taskname)):
        if os.path.exists(worker_base):
            shutil.rmtree(worker_base, ignore_errors=True)
        os.makedirs(worker_base, exist_ok=True)
        _run("7z x -y {}/{}.iso -o{}".format(app.config['OEM_MM_ISO_DIR'],
                                             taskname, worker_base), False)

    logging.info("Worker-{}#copy {}to{}".format(os.getpid(),
                                                taskname, worker_base))

    if not os.path.exists("{}/oem/deb".format(worker_base)):
        os.makedirs("{}/oem/deb".format(worker_base), exist_ok=True)
    _run("cp -v {}/../files/*.deb {}/oem/deb/".format(worker_base, worker_base))

    # print("Worker#pack\n", oem_maker_task)

    task_info.status = "pack"
    task_info.update_at = datetime.datetime.utcnow()
    db.session.add(task_info)
    db.session.commit()

    output = "{}/{}/{}-fix1.iso".format(
        app.config['OEM_MM_TASK_WORKBASE'], taskname, taskname)

    logging.info("Worker-{}#pack {}to{}".format(os.getpid(),
                                                taskname, output))
    _run('''xorriso -as mkisofs -rational-rock -joliet -follow-links -eltorito-catalog boot.cat -boot-load-size 4 -boot-info-table -eltorito-alt-boot --efi-boot boot/grub/efi.img -no-emul-boot -V "uos 20" -file_name_limit 250 -o {} {}'''.format(output, worker_base))
    # time.sleep(10)

    time.sleep(3)

    # print("Worker#finish\n", oem_maker_task)
    logging.info("Worker-{}#finish !".format(os.getpid()))
    task_info.status = "finish"
    task_info.update_at = datetime.datetime.utcnow()
    task_info.active = False
    db.session.add(task_info)
    db.session.commit()

    maker_cancel()
    _clear()


@ app.route('/submit/<taskname>', methods=['GET', 'POST'])
@ login_required
def submit(taskname):
    logging.debug("submit task {} {}".format(taskname, request.method))

    def put(t=oem_maker_task, n=taskname):
        if n not in t:
            lock.acquire()
            t.append(n)
            lock.release()
            return True
        else:
            return False
    job_put = put

    rc_info = "任务:{}".format(taskname)
    task_info = None
    try:
        task_info = db.session.query(Taskinfo).filter(
            Taskinfo.name == taskname).one()
    except:
        logging.error("submit task {} err {} : query taskinfo".format(
            taskname, request.method))

    if not task_info:
        logging.error("submit task {} err {} : not found taskinfo".format(
            taskname, request.method))

    if request.method == 'POST':
        logging.debug("submit new taskinfo {} {}".format(
            taskname, request.method))
        task_info.status = "start"
        task_info.active = True
        task_info.update_at = datetime.datetime.utcnow()
        db.session.add(task_info)
        db.session.commit()

        if job_put():
            logging.debug("submit put task {} {} to worker_iso_oem_maker".format(
                taskname, request.method))
            worker_process = Process(
                name="worker_iso_oem_maker",
                target=iso_oem_maker,
                args=(oem_maker_task, taskname),
                daemon=True
            )
            worker_process.start()
            # worker_process.join()
        else:
            rc_info = "任务提交成功:{}".format(taskname)
    elif request.method == "GET":
        rc_info = "任务已经提交:{} / 状态:{}".format(taskname, task_info.status)
    logging.debug("submit task {} {} list:[ {} ]".format(
        taskname, request.method, " ".join(oem_maker_task)))
    return render_template('oem-task.html', taskinfo=task_info, result=rc_info)


@ app.route('/publish/<taskname>', methods=['GET', 'POST'])
@ login_required
def publish(taskname):
    logging.debug("publish task {} {}".format(taskname, request.method))
    try:
        task_info = db.session.query(Taskinfo).filter(
            Taskinfo.name == taskname).one()
    except:
        # logging.error("publish task {} {}".format(taskname, request.method))
        logging.error("publish task {} err {} : select taskinfo".format(
            taskname, request.method))
    if not task_info:
        logging.error("publish task {} err {} : not found taskinfo".format(
            taskname, request.method))

    worker_base = "{}/{}".format(
        app.config['OEM_MM_TASK_WORKBASE'], taskname)

    for flist in os.listdir(worker_base):
        if os.path.isfile("{}/{}".format(worker_base, flist)) and os.path.splitext(flist)[1] == ".iso":
            logging.info("publish task {} {} workbase: {} on {}".format(
                taskname, request.method, worker_base, flist))
            shutil.move("{}/{}".format(worker_base, flist), "{}/{}".format(
                app.config['OEM_MM_ISO_DIR'], flist.replace("fix1", time.strftime("%Y%m%d-%H%M%S", time.localtime()))))
    return redirect(url_for('nindex'))


@ app.route('/nindex', methods=['GET'])
@ login_required
def nindex():
    def _file_iso(name):
        return os.path.splitext(name)[0]
    if request.method == 'GET':
        idx_show_file_list = []

        if app.config['OEM_MM_ISO_DIR']:
            for flist in os.listdir(app.config['OEM_MM_ISO_DIR']):
                (_, _, _, _, _, _, size,
                 _, _, ctime) = os.stat("{}/{}".format(app.config['OEM_MM_ISO_DIR'], flist))
                idx_show_file_list.append(
                    (_file_iso(flist), time.ctime(ctime), size / 1024 / 1024 / 1024))
        return render_template('nindex.html', idx_file_list=idx_show_file_list)


if __name__ == '__main__':
    if len(sys.argv) > 1 and sys.argv[1] == "initdb":
        build_sample_db()
        sys.exit(0)
        if not os.path.exists(os.path.join(os.path.realpath(os.path.dirname(__file__)), app.config['DATABASE_FILE'])):
            build_sample_db()
    else:
        if not os.path.exists(os.path.join(os.path.realpath(os.path.dirname(__file__)), app.config['DATABASE_FILE'])):
            build_sample_db()
        app.run()
