# debug mode
# debug = True
import logging

# DEBUG = 1
# SERVER_NAME = "0.0.0.0:8080"
# Create dummy secrey key so we can use sessions
SECRET_KEY = '123456790'

# Create in-memory database
DATABASE_FILE = 'demo_db.sqlite'
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + DATABASE_FILE
# SQLALCHEMY_ECHO = True

# Flask-Security config
SECURITY_URL_PREFIX = "/admin"
SECURITY_PASSWORD_HASH = "pbkdf2_sha512"
SECURITY_PASSWORD_SALT = "TN9WPD1L85mAKm6BRLddf6QSVHdV3vwJnTrhUQ"

# Flask-Security URLs, overridden because they don't put a / at the end
SECURITY_LOGIN_URL = "/login/"
SECURITY_LOGOUT_URL = "/logout/"
SECURITY_REGISTER_URL = "/register/"

SECURITY_POST_LOGIN_VIEW = "/admin/"
SECURITY_POST_LOGOUT_VIEW = "/admin/"
SECURITY_POST_REGISTER_VIEW = "/admin/"

# Flask-Security features
SECURITY_REGISTERABLE = True
SECURITY_SEND_REGISTER_EMAIL = False
SQLALCHEMY_TRACK_MODIFICATIONS = False

# Flask Uploads
UPLOADS_DEFAULT_DEST = 'uploads'

# OEM
OEM_MM_ISO_DIR = '/srv/klu-oem-iso'
OEM_MM_TASK_WORKBASE = "/srv/taskbasedir"

OEM_LOGGING_LEVEL = logging.DEBUG
OEM_LOGGING_FILE = "oem-web.log"
OEM_LOGGING_FORMAT = "%(asctime)s %(name)s:%(levelname)s:%(message)s"
OEM_LOGGING_DATEFMT = "%d-%M-%Y %H:%M:%S"
OEM_LOGGING = 'oem-web'
